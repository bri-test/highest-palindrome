package main

import (
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
)

// Fungsi utama untuk menemukan highest palindrome
func highestPalindrome(s string, k int) string {
	// Konversi string menjadi array rune untuk memudahkan pengubahan karakter
	runes := []rune(s)
	n := len(runes)

	// Fungsi rekursif untuk membuat palindrom
	var makePalindrome func([]rune, int, int, int) bool
	makePalindrome = func(runes []rune, left int, right int, k int) bool {
		if left >= right {
			return true
		}
		if runes[left] != runes[right] {
			if k == 0 {
				return false
			}
			if runes[left] > runes[right] {
				runes[right] = runes[left]
			} else {
				runes[left] = runes[right]
			}
			k--
		}
		return makePalindrome(runes, left+1, right-1, k)
	}

	// Cek apakah bisa membuat palindrom dengan k perubahan
	if !makePalindrome(runes, 0, n-1, k) {
		return "-1"
	}

	// Fungsi rekursif untuk memaksimalkan nilai palindrom
	var maximizePalindrome func([]rune, int, int, int) bool
	maximizePalindrome = func(runes []rune, left int, right int, k int) bool {
		if left >= right {
			return true
		}
		if runes[left] < '9' {
			if runes[left] == runes[right] {
				if k >= 2 {
					runes[left] = '9'
					runes[right] = '9'
					k -= 2
				}
			} else {
				if k >= 1 {
					runes[left] = '9'
					runes[right] = '9'
					k--
				}
			}
		}
		return maximizePalindrome(runes, left+1, right-1, k)
	}

	// Maksimalkan nilai palindrom dengan k perubahan yang tersisa
	maximizePalindrome(runes, 0, n-1, k)

	return string(runes)
}

func RunSample() {
	sample1 := "3943"
	k1 := 1
	fmt.Println("Sample 1: inputStr =", sample1, ", k =", k1, ", hasil : ", highestPalindrome(sample1, k1))

	sample2 := "932239"
	k2 := 2
	fmt.Println("Sample 2: inputStr =", sample2, ", k =", k2, ", hasil : ", highestPalindrome(sample2, k2))
}

func main() {
	RunSample()
	var inputStr, inputK string
	fmt.Println("input string : ")
	fmt.Scanln(&inputStr)
	fmt.Println("input k : ")
	fmt.Scanln(&inputK)
	isMatch, _ := regexp.MatchString("[0-9]", inputK)
	if !isMatch {
		log.Println("k harus di isi dengan angka")
		return
	}
	k, _ := strconv.Atoi(strings.TrimSpace(inputK))
	log.Println(inputStr, inputK)
	fmt.Println("inputStr =", inputStr, ", k =", k, ", hasil : ", highestPalindrome(inputStr, k))

}
